
const express = require('express');
const data = require('./lib/data');
const app = express();

const getData = async() =>{
  app.set('info', await data.update());
}

app.get('/', async (req, res) => {
    return res.send(app.get('info'));
});

app.listen(3000, async() => {
  app.set('info', await data.update());
  setInterval(getData, 60000);
  console.log('Example app listening on port 3000!');
});

