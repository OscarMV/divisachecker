const hour = require('./hour');
const traditional = require('./traditional');
const crypto = require('./crypto');

module.exports = {
    update: async() => {
        let datetime = {"datetime": await hour.get()};
        let traditionalMoney = await traditional.get();
        let cryptoCurrency = await crypto.get();
        let data = {...datetime, ...traditionalMoney, ...cryptoCurrency};
        return data;
    }
};