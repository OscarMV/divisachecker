const request = require('request-promise');

module.exports = {
    get: async() => {
        let opt = {
            uri: 'http://worldtimeapi.org/api/timezone/Etc/UTC',
            method: 'GET',
            json: true
        }
        try {
            let { datetime } = await request(opt);
            return datetime;
        } catch (err) {
            console.error(err);
        }
    }
};