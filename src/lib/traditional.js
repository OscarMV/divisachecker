const request = require('request-promise');

module.exports = {
    get: async() => {
        let opt = {
            uri: 'https://api.exchangeratesapi.io/latest?base=USD',
            method: 'GET',
            json: true
        }
        try {
            let { rates } = await request(opt);
            return rates;
        } catch (err) {
            console.error(err);
        }
    }
};