const request = require('request-promise');

module.exports = {
    get: async() => {
        let opt = {
            uri: 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=bitcoin%2Cethereum%2Cripple%2Ctether%2Cbitcoin-cash%2Clitecoin%2Cbinancecoin&order=market_cap_desc&per_page=100&page=1&sparkline=false',
            method: 'GET',
            json: true
        }
        try {
            let data = await request(opt);
            let response = {};
            data.forEach(crypto => {
                response[crypto.symbol.toUpperCase()] = crypto.current_price
            });
            return response;
        } catch (err) {
            console.error(err);
        }
    }
};